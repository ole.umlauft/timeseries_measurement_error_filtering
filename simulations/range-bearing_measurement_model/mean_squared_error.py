import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import norm
import pickle

from src.filters import lstm_network, rnn_network, extended_kalman_filter

'''
This calculates and shows the mean squared error for lstm network, rnn network, kalman filter and measurements.
'''

HIGH = True
sensor = np.array([10, -10])

if HIGH:
    name = "high"
    mR = np.diag([0.05, 0.01])
else:
    name = "low"
    mR = np.diag([0.01, 0.002])

lstm = lstm_network.load_model(f'weights/lstm_{name}_weights.h5', input_shape=(None, 2))
rnn = rnn_network.load_model(f'weights/rnn_{name}_weights.h5', input_shape=(None, 2))

test_tj = np.array(pickle.load(open(f'data/test_tj.pkl', 'rb')))
test_ms = np.array(pickle.load(open(f'data/{name}/test_ms.pkl', 'rb')))
test_pd = np.array(pickle.load(open(f'data/test_pd.pkl', 'rb')))

def lstm_filter(model, measurements):
    lstm_batches = []
    for batch in measurements:
        lstm_estimation = model.predict(batch, batch_size=100)
        lstm_batches.append(lstm_estimation)
    return np.array(lstm_batches)

def rnn_filter(model, measurements):
    rnn_batches = []
    for batch in measurements:
        rnn_estimation, _ = model.predict(batch, batch_size=100)
        rnn_batches.append(rnn_estimation)
    return np.array(rnn_batches)

def mse(tj, est):
    mse_position = norm(tj[:, :, :2] - est[:, :, :2], axis=-1)
    mse_position = np.sum(mse_position, axis=0) / tj.shape[0]
    if est.shape[-1] > 2:
        mse_velocity = norm(tj[:, :, 2:] - est[:, :, 2:], axis=-1)
        mse_velocity = np.sum(mse_velocity, axis=0) / tj.shape[0]
        return mse_position, mse_velocity
    return mse_position

test_cms = []
for batch in test_ms:
    new_batch = []
    for i_ms in batch:
        new_batch.append(np.array([np.cos(i_ms[:, 1]), np.sin(i_ms[:, 1])]).T * i_ms[:, 0, None] + sensor)
    test_cms.append(np.array(new_batch))
test_cms = np.array(test_cms)

lstm_esti = lstm_filter(lstm, test_ms)
rnn_esti = rnn_filter(rnn, test_cms)

test_ms = np.reshape(test_ms, newshape=(test_ms.shape[0]*test_ms.shape[1],
                                        test_ms.shape[2], test_ms.shape[3]))
kf_esti = extended_kalman_filter.batch_ekf(test_ms, sensor, mR)

test_ms = np.reshape(test_cms, newshape=(test_cms.shape[0]*test_cms.shape[1],
                                         test_cms.shape[2], test_cms.shape[3]))


lstm_esti = np.reshape(lstm_esti, newshape=(lstm_esti.shape[0]*lstm_esti.shape[1],
                                            lstm_esti.shape[2], lstm_esti.shape[3]))
rnn_esti = np.reshape(rnn_esti, newshape=(rnn_esti.shape[0]*rnn_esti.shape[1],
                                          rnn_esti.shape[2], rnn_esti.shape[3]))
test_tj = np.reshape(test_tj, newshape=(test_tj.shape[0]*test_tj.shape[1],
                                        test_tj.shape[2], test_tj.shape[3]))

ms_mse_pos = mse(test_tj, test_ms)
lstm_mse_pos, lstm_mse_vel = mse(test_tj, lstm_esti)
rnn_mse_pos, rnn_mse_vel = mse(test_tj, rnn_esti)
kf_mse_pos, kf_mse_vel = mse(test_tj, kf_esti)


plt.figure(1, figsize=(10, 10))
plt.plot(ms_mse_pos, label='measurements position')
plt.plot(kf_mse_pos, label='extended kalman filter position')
plt.plot(lstm_mse_pos, label='lstm filter position')
plt.plot(rnn_mse_pos, label='rnn filter position')
plt.legend()
plt.xlabel("time")
plt.ylabel("mse")

plt.figure(2, figsize=(10, 10))
plt.plot(kf_mse_vel, label='extended kalman filter velocity', color="C1")
plt.plot(lstm_mse_vel, label=f'lstm filter velocity', color="C2")
plt.plot(rnn_mse_vel, label=f'rnn filter velocity', color="C3")
plt.legend()
plt.xlabel("time")
plt.ylabel("mse")
plt.show()
