import numpy as np
import pickle
import os

from src.filters import lstm_network, rnn_network

'''
This shows an example for training the lstm network and the rnn network.
'''

HIGH = True
sensor = np.array([10, -10])

if HIGH:
    name = "high"
else:
    name = "low"

lstm = lstm_network.load_model(None, input_shape=(None, 2))
rnn = rnn_network.load_model(None, input_shape=(None, 2))

train_tj = np.array(pickle.load(open(f'data/train_tj.pkl', 'rb')))
train_ms = np.array(pickle.load(open(f'data/{name}/train_ms.pkl', 'rb')))
train_pd = np.array(pickle.load(open(f'data/train_pd.pkl', 'rb')))

train_tj = np.reshape(train_tj, (train_tj.shape[0]*train_tj.shape[1],
                                 train_tj.shape[2], train_tj.shape[3]))
train_ms = np.reshape(train_ms, (train_ms.shape[0]*train_ms.shape[1],
                                 train_ms.shape[2], train_ms.shape[3]))
train_pd = np.reshape(train_pd, (train_pd.shape[0]*train_pd.shape[1],
                                 train_pd.shape[2], train_pd.shape[3]))

history_lstm = lstm.fit(x=train_ms, y=train_tj, batch_size=100, validation_split=0.1, epochs=100)

if not os.path.exists('weights/'):
    os.mkdir('weights/')

lstm.save_weights(f'weights/lstm_{name}_weights.h5')

y_trains = {
    'updated_output': train_tj,
    'predicted_output': train_pd
}
train_cms = []
for i_ms in train_ms:
    train_cms.append(np.array([np.cos(i_ms[:, 1]), np.sin(i_ms[:, 1])]).T * i_ms[:, 0, None] + sensor)
train_cms = np.array(train_cms)

history_rnn = rnn.fit(x=train_cms, y=y_trains, batch_size=100, validation_split=0.1, epochs=100)
rnn.save_weights(f'weights/rnn_{name}_weights.h5')
