import numpy as np
import pickle
import matplotlib.pyplot as plt

from src.filters import lstm_network, rnn_network, extended_kalman_filter

'''
This shows an example trajectory with certain measurements and filterings.
'''

HIGH = True
sensor = np.array([10, -10])

if HIGH:
    name = "high"
    mR = np.diag([0.05, 0.01])
else:
    name = "low"
    mR = np.diag([0.01, 0.002])

lstm = lstm_network.load_model(f'weights/lstm_{name}_weights.h5', input_shape=(None, 2))
rnn = rnn_network.load_model(f'weights/rnn_{name}_weights.h5', input_shape=(None, 2))

test_tj = np.array(pickle.load(open(f'data/test_tj.pkl', 'rb')))
test_ms = np.array(pickle.load(open(f'data/{name}/test_ms.pkl', 'rb')))
test_pd = np.array(pickle.load(open(f'data/test_pd.pkl', 'rb')))


# Random choice
i, j = (np.random.randint(0, test_tj.shape[0]), np.random.randint(0, test_tj.shape[1]))

# Estimations
lstm_esti = lstm.predict(test_ms[i], batch_size=100)
test_cms = []
for i_ms in test_ms[i]:
    test_cms.append(np.array([np.cos(i_ms[:, 1]), np.sin(i_ms[:, 1])]).T * i_ms[:, 0, None] + sensor)
test_cms = np.array(test_cms)
rnn_esti, _ = rnn.predict(test_cms, batch_size=100)
kf_esti = extended_kalman_filter.extended_kalman_filter(test_ms[i][j], sensor)

plt.figure(1)

plt.scatter(test_tj[i, j, :, 0], test_tj[i, j, :, 1], label="ground truth")
plt.plot(test_tj[i, j, :, 0], test_tj[i, j, :, 1])
plt.scatter(test_cms[j, :, 0], test_cms[j, :, 1], label="measurements")
plt.plot(test_cms[j, :, 0], test_cms[j, :, 1])
plt.scatter(kf_esti[:, 0], kf_esti[:, 1], label="kalman filtering")
plt.plot(kf_esti[:, 0], kf_esti[:, 1])
plt.scatter(lstm_esti[j, :, 0], lstm_esti[j, :, 1], label="lstm filtering")
plt.plot(lstm_esti[j, :, 0], lstm_esti[j, :, 1])
plt.scatter(rnn_esti[j, :, 0], rnn_esti[j, :, 1], label="rnn filtering")
plt.plot(rnn_esti[j, :, 0], rnn_esti[j, :, 1])
plt.scatter(sensor[0], sensor[1], label="sensor")
plt.legend()

plt.xlabel("x")
plt.ylabel("y")

plt.show()
