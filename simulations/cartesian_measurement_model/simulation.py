import numpy as np
import pickle
import matplotlib.pyplot as plt

from src.filters import lstm_network, rnn_network, kalman_filter

'''
This shows an example trajectory with certain measurements and filterings.
'''

lstm = lstm_network.load_model('weights/lstm_weights.h5', input_shape=(None, 4))
rnn = rnn_network.load_model('weights/rnn_weights.h5', input_shape=(None, 4))

test_tj = np.array(pickle.load(open('data/test_tj.pkl', 'rb')))
test_ms = np.array(pickle.load(open('data/test_ms.pkl', 'rb')))
test_pd = np.array(pickle.load(open('data/test_pd.pkl', 'rb')))


# Random choice
i, j = (np.random.randint(0, test_tj.shape[0]), np.random.randint(0, test_tj.shape[1]))

# Estimations
lstm_esti = lstm.predict(test_ms[i], batch_size=100)
rnn_esti, _ = rnn.predict(test_ms[i], batch_size=100)
kf_esti = kalman_filter.filter(test_ms[i][j], dt=1)

plt.figure(1)

plt.scatter(test_tj[i, j, :, 0], test_tj[i, j, :, 1], label="ground_truth")
plt.plot(test_tj[i, j, :, 0], test_tj[i, j, :, 1])
plt.scatter(test_ms[i, j, :, 0], test_ms[i, j, :, 1], label="measurements")
plt.plot(test_ms[i, j, :, 0], test_ms[i, j, :, 1])
plt.scatter(kf_esti[:, 0], kf_esti[:, 1], label="kalman filtering")
plt.plot(kf_esti[:, 0], kf_esti[:, 1])
plt.scatter(lstm_esti[j, :, 0], lstm_esti[j, :, 1], label="lstm filtering")
plt.plot(lstm_esti[j, :, 0], lstm_esti[j, :, 1])
plt.scatter(rnn_esti[j, :, 0], rnn_esti[j, :, 1], label="rnn filtering")
plt.plot(rnn_esti[j, :, 0], rnn_esti[j, :, 1])
plt.legend()

plt.xlabel("x")
plt.ylabel("y")

plt.show()
