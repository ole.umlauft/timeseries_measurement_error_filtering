import matplotlib.pyplot as plt
import numpy as np
from numpy.linalg import norm
import pickle

from src.filters import lstm_network, rnn_network, kalman_filter

'''
This calculates and shows the mean squared error for lstm network, rnn network, kalman filter and measurements.
'''

lstm = lstm_network.load_model('weights/lstm_weights.h5', input_shape=(None, 4))
rnn = rnn_network.load_model('weights/rnn_weights.h5', input_shape=(None, 4))

test_tj = np.array(pickle.load(open('data/test_tj.pkl', 'rb')))
test_ms = np.array(pickle.load(open('data/test_ms.pkl', 'rb')))
test_pd = np.array(pickle.load(open('data/test_pd.pkl', 'rb')))

def lstm_filter(model, measurements):
    lstm_batches = []
    for batch in measurements:
        lstm_estimation = model.predict(batch, batch_size=100)
        lstm_batches.append(lstm_estimation)
    return np.array(lstm_batches)

def rnn_filter(model, measurements):
    rnn_batches = []
    for batch in measurements:
        rnn_estimation, _ = model.predict(batch, batch_size=100)
        rnn_batches.append(rnn_estimation)
    return np.array(rnn_batches)

def mse(tj, est):
    mse_position = norm(tj[:, :, :2] - est[:, :, :2], axis=-1)
    mse_position = np.sum(mse_position, axis=0) / tj.shape[0]
    mse_velocity = norm(tj[:, :, 2:] - est[:, :, 2:], axis=-1)
    mse_velocity = np.sum(mse_velocity, axis=0) / tj.shape[0]
    return mse_position, mse_velocity


lstm_esti = lstm_filter(lstm, test_ms)
rnn_esti = rnn_filter(rnn, test_ms)

test_ms = np.reshape(test_ms, newshape=(test_ms.shape[0]*test_ms.shape[1],
                                        test_ms.shape[2], test_ms.shape[3]))
kf_esti = kalman_filter.batch_kf(test_ms, dt=1)

lstm_esti = np.reshape(lstm_esti, newshape=(lstm_esti.shape[0]*lstm_esti.shape[1],
                                            lstm_esti.shape[2], lstm_esti.shape[3]))
rnn_esti = np.reshape(rnn_esti, newshape=(rnn_esti.shape[0]*rnn_esti.shape[1],
                                          rnn_esti.shape[2], rnn_esti.shape[3]))
test_tj = np.reshape(test_tj, newshape=(test_tj.shape[0]*test_tj.shape[1],
                                        test_tj.shape[2], test_tj.shape[3]))

ms_mse_pos, ms_mse_vel = mse(test_tj, test_ms)
lstm_mse_pos, lstm_mse_vel = mse(test_tj, lstm_esti)
rnn_mse_pos, rnn_mse_vel = mse(test_tj, rnn_esti)
kf_mse_pos, kf_mse_vel = mse(test_tj, kf_esti)


plt.figure(1, figsize=(10, 10))
plt.plot(ms_mse_pos, label='measurements position')
plt.plot(kf_mse_pos, label='kalman filter position')
plt.plot(lstm_mse_pos, label='lstm filter position')
plt.plot(rnn_mse_pos, label='rnn filter position')
plt.legend()
plt.xlabel("time")
plt.ylabel("mse")

plt.figure(2, figsize=(10, 10))
plt.plot(ms_mse_vel, label = 'measurements velocity')
plt.plot(kf_mse_vel, label='kalman filter velocity')
plt.plot(lstm_mse_vel, label=f'lstm filter velocity')
plt.plot(rnn_mse_vel, label=f'rnn filter velocity')
plt.legend()
plt.xlabel("time")
plt.ylabel("mse")
plt.show()
