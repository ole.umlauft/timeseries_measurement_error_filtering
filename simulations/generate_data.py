import numpy as np
from numpy.linalg import norm
from numpy.random import multivariate_normal as mvn
import os
from tqdm import tqdm
import pickle


def get_data(init, init_cov, sigma_q, m_cov_cartesian, m_cov_rangebearing_low, m_cov_rangebearing_high, dt, steps, sensor):
    '''
    Generates a trajectory and a measurement for each time step.
    :param init:            expected initial position and velocity of track (x, y, vx, vy)
    :param init_cov:        uncertainty of initial position (4,4)
    :param sigma_q:         transition noise on x- and y-velocity
    :param dt:              length of a time step
    :param m_cov_cartesian: cartesian measurement noise covariance (4,4)
    :param m_cov_rangebearing_low: low range and bearing measurement noise covariance (2,2)
    :param m_cov_rangebearing_high: high range and bearing measurement noise covariance (2,2)
    :param steps:           length of trajectory
    '''
    # trajectory setup
    traj = np.zeros((steps, 4))
    traj[0] = init + mvn(np.zeros(4), init_cov)  # initial position
    trans = np.array([[1, 0, dt, 0],
                      [0, 1, 0, dt],
                      [0, 0, 1, 0],
                      [0, 0, 0, 1]])  # transition matrix
    g = np.array([[dt ** 2 / 2, 0],
                  [0, dt ** 2 / 2],
                  [dt, 0],
                  [0, dt]])

    trans_cov = np.einsum('ab, bc, dc -> ad', g, np.diag(sigma_q), g)  # transition error covariance

    print(trans_cov)
    # measurement setup
    cartesian_ms = np.zeros((steps, 4))
    cartesian_ms[0] = traj[0] + mvn(np.zeros(4), m_cov_cartesian)

    rangebearing_low_ms = np.zeros((steps, 2))
    rangebearing_low_ms[0] = detect(traj[0, :2] - sensor, m_cov_rangebearing_low)
    rangebearing_high_ms = np.zeros((steps, 2))
    rangebearing_high_ms[0] = detect(traj[0, :2] - sensor, m_cov_rangebearing_high)

    # generate move object and generate measurement
    pred = np.zeros((steps, 4))
    for i in range(1, steps):
        traj[i] = np.dot(trans, traj[i - 1])
        traj[i] += mvn(np.zeros(4), trans_cov)
        pred[i-1] = traj[i]
        cartesian_ms[i] = traj[i] + mvn(np.zeros(4), m_cov_cartesian)
        rangebearing_low_ms[i] = detect(traj[i, :2] - sensor, m_cov_rangebearing_low)
        rangebearing_high_ms[i] = detect(traj[i, :2] - sensor, m_cov_rangebearing_high)
    pred[i] = np.dot(trans, traj[i]) + mvn(np.zeros(4), trans_cov)

    return traj, pred, cartesian_ms, rangebearing_high_ms, rangebearing_low_ms

def detect(x, m_cov):
    '''
    Create a measurement from x
    :param x:        detected point relative to the sensor
    :param m_cov:      measurement error covariance
    '''
    m = np.zeros(2)
    m[0] = norm(x)
    m[1] = np.arctan2(x[1], x[0])
    m += mvn(np.zeros(2), m_cov)
    return m



def get_batch(batch_size, init_cov, sigma_q, m_cov_cartesian, m_cov_rangebearing_low, m_cov_rangebearing_high, dt, steps, sensor, width=20):
    init = (np.random.rand(4) - 0.5) * width
    init[2] = 1
    init[3] = 1

    batch_ms = np.zeros((batch_size, steps, 4))
    batch_traj = np.zeros((batch_size, steps, 4))
    batch_pred = np.zeros((batch_size, steps, 4))
    batch_rb_low = np.zeros((batch_size, steps, 2))
    batch_rb_high = np.zeros((batch_size, steps, 2))
    for i in range(batch_size):
        batch_traj[i], batch_pred[i], batch_ms[i], batch_rb_low[i], batch_rb_high[i] \
            = get_data(init, init_cov, sigma_q, m_cov_cartesian, m_cov_rangebearing_low, m_cov_rangebearing_high, dt, steps, sensor)
    return batch_traj, batch_pred, batch_ms, batch_rb_low, batch_rb_high


def batch_generator(n, batch_size, init_cov, sigma_q, m_cov_cartesian, m_cov_rangebearing_low, m_cov_rangebearing_high, dt, steps, sensor, width):
    i = 0
    while i < n:
        yield get_batch(batch_size, init_cov, sigma_q, m_cov_cartesian, m_cov_rangebearing_low, m_cov_rangebearing_high, dt, steps, sensor, width)
        i += 1

def generate_data(n=100,
                  batch_size=100,
                  init_cov=np.identity(4)*0.05**2,
                  sigma_q=np.array([0.1, 0.1]) ** 2,
                  sensor=np.array([4, 0]),
                  m_cov_cartesian=np.diag((0.1, 0.1, 0.01, 0.01)),
                  m_cov_rangebearing_low=np.diag((0.01, 0.002)),
                  m_cov_rangebearing_high=np.diag((0.05, 0.01)),
                  dt=1, steps=10, width=20):

    list_traj = []
    list_pred = []
    list_ms = []
    list_rb_low = []
    list_rb_high = []

    g_tj = batch_generator(n=n,
                           batch_size=batch_size,
                           init_cov=init_cov,
                           sigma_q=sigma_q,
                           m_cov_cartesian=m_cov_cartesian,
                           m_cov_rangebearing_low=m_cov_rangebearing_low,
                           m_cov_rangebearing_high=m_cov_rangebearing_high,
                           dt=dt, steps=steps,
                           sensor=sensor,
                           width=width)
    for i in tqdm(range(n)):
        batch_traj, batch_pred, batch_ms, batch_rb_low, batch_rb_high = next(g_tj)
        list_traj.append(batch_traj)
        list_pred.append(batch_pred)
        list_ms.append(batch_ms)
        list_rb_low.append(batch_rb_low)
        list_rb_high.append(batch_rb_high)

    return list_traj, list_pred, list_ms, list_rb_low, list_rb_high


if __name__ == "__main__":
    path_ca = "cartesian_measurement_model/data"
    path_rb = "range-bearing_measurement_model/data"

    if not os.path.exists(path_ca):
        os.mkdir(path_ca)
    if not os.path.exists(path_rb):
        os.mkdir(path_rb)
    if not os.path.exists(f"{path_ca}/low"):
        os.mkdir(f"{path_ca}/low")
    if not os.path.exists(f"{path_ca}/high"):
        os.mkdir(f"{path_ca}/high")

    list_traj, list_pred, list_ms, list_rb_low, list_rb_high = generate_data(n=1)
    pickle.dump(list_traj, open(f'{path_ca}/train_tj.pkl', 'wb'))
    pickle.dump(list_pred, open(f'{path_ca}/train_pd.pkl', 'wb'))
    pickle.dump(list_traj, open(f'{path_rb}/train_tj.pkl', 'wb'))
    pickle.dump(list_pred, open(f'{path_rb}/train_pd.pkl', 'wb'))
    pickle.dump(list_ms, open(f'{path_ca}/train_ms.pkl', 'wb'))
    pickle.dump(list_rb_low, open(f'{path_rb}/low/train_ms.pkl', 'wb'))
    pickle.dump(list_rb_high, open(f'{path_rb}/high/train_ms.pkl', 'wb'))

    list_traj, list_pred, list_ms, list_rb_low, list_rb_high = generate_data(n=1)
    pickle.dump(list_traj, open(f'{path_ca}/test_tj.pkl', 'wb'))
    pickle.dump(list_pred, open(f'{path_ca}/test_pd.pkl', 'wb'))
    pickle.dump(list_traj, open(f'{path_rb}/test_tj.pkl', 'wb'))
    pickle.dump(list_pred, open(f'{path_rb}/test_pd.pkl', 'wb'))
    pickle.dump(list_ms, open(f'{path_ca}/test_ms.pkl', 'wb'))
    pickle.dump(list_rb_low, open(f'{path_rb}/low/test_ms.pkl', 'wb'))
    pickle.dump(list_rb_high, open(f'{path_rb}/high/test_ms.pkl', 'wb'))