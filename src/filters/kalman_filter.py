import numpy as np
from tqdm import tqdm

'''
This code based on this "Implementation of Kalman Filter with Python Language".
https://arxiv.org/pdf/1204.0375.pdf
'''


def kf_predict(X, P, A, Q, B, U):
    '''
    Kalman filter prediction step.
    :param X: The mean state estimate of the previous step (k-1).
    :param P: The state covariance of the previous step (k-1).
    :param A: The transition n x n matrix.
    :param Q: The process noise covariance matrix.
    :param B: The input effect matrix.
    :param U: The control Input.
    :return: (predicted state, predicted state covariance)
    '''
    X = np.dot(A, X) + np.dot(B, U)
    P = np.dot(A, np.dot(P, A.T)) + Q
    return (X, P)


def kf_update(X, P, Y, H, R):
    '''
    Kalman filter update step.
    Additional parameters:
    K -- The kalman gain matrix.
    IM -- The mean of predictive distribution of Y.
    IS -- The Covariance or predictive mean of Y.
    LH -- The predictive probability (likelihood)
          of measurement which is computed using the
          function gauss_pdf.

    :param X: The mean state estimate.
    :param P: The state covariance.
    :param Y: The measurement vector.
    :param H: The measurement matrix H.
    :param R: The measurement covariance matrix.
    :return:

    '''
    IM = np.dot(H, X)
    IS = R + np.dot(H, np.dot(P, H.T))
    K = np.dot(P, np.dot(H.T, np.linalg.inv(IS)))
    X = X + np.dot(K, (Y-IM))
    P = P - np.dot(K, np.dot(IS, K.T))
    return (X, P, K, IM, IS)


def filter(ms, dt):
    X = np.reshape(ms[0], (ms[0].shape[0], 1))
    P = np.diag((dt, dt, dt, dt))
    A = np.array([[1.0, 0.0, dt, 0.0],
                  [0.0, 1.0, 0.0, dt],
                  [0.0, 0.0, 1.0, 0.0],
                  [0.0, 0.0, 0.0, 1.0]])
    Q = np.array([[0.01, 0., 0.02, 0.],
                  [0., 0.01, 0., 0.02],
                  [0.02, 0., 0.04, 0.],
                  [0., 0.02, 0., 0.04]])  # np.diag((0.1, 0.1, 0.01, 0.01))
    B = np.eye(X.shape[0])
    U = np.zeros((X.shape[0], 1))

    ms = np.reshape(ms, newshape=(ms.shape[0], ms.shape[1], 1))
    H = np.array([[1, 0, 0, 0],
                  [0, 1, 0, 0],
                  [0, 0, 1, 0],
                  [0, 0, 0, 1]])
    R = np.diag((0.1, 0.1, 0.01, 0.01))  # np.eye(ms.shape[1])

    updated = [X]
    for Y in ms[1:]:
        (X, P) = kf_predict(X, P, A, Q, B, U)
        (X, P, K, IM, IS) = kf_update(X, P, Y, H, R)
        updated.append(X)
    updated = np.array(updated)
    updated = np.reshape(updated, newshape=(updated.shape[0], updated.shape[1]))
    return updated

def batch_kf(batch, dt):
    updated = []
    for ms in tqdm(batch):
        updated.append(filter(ms, dt))
    return np.array(updated)